#pragma once
#ifndef CSOMMET_H
#define CSOMMET_H

#include "CArc.h"
#include "CException.h"
#include "CTableauDynamique.h"

#define EXCAllocationFailed		10
#define EXCArcNotFound			11

class CSommet
{
private:

	unsigned int uiSOMID; // Numero du sommet

	CTableauDynamique<CArc> ARCSOMArcsArrivants;

	CTableauDynamique<CArc> ARCSOMArcsPartants;

public:

	/*************************************************************************
	Constructeur par d�faut de la classe CSommet
	/*************************************************************************
	****** Entr�e : Rien
	****** Sortie : Rien
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	CSommet();


	/*************************************************************************
	Constructeur de la classe CSommet avec ID sp�cifi�
	/*************************************************************************
	****** Entr�e : Entier non sign� (uiID) repr�sentant l'ID du sommet
	****** Sortie : Rien
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	CSommet(unsigned int uiID);


	/*************************************************************************
	Constructeur de copie de la classe CSommet
	/*************************************************************************
	****** Entr�e : Objet CSommet (SOMArg) � copier
	****** Sortie : Rien
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	CSommet(CSommet & SOMArg);


	/*************************************************************************
	Destructeur de la classe CSommet
	/*************************************************************************
	****** Entr�e : Rien
	****** Sortie : Rien
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	~CSommet();


	/*************************************************************************
	Accesseur pour obtenir l'ID du sommet
	/*************************************************************************
	****** Entr�e : Rien
	****** Sortie : Entier non sign� repr�sentant l'ID du sommet
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	unsigned int SOMGetID() const;

private :
	

	/*************************************************************************
	Accesseur pour obtenir un arc arrivant sp�cifique
	/*************************************************************************
	****** Entr�e : Entier non sign� (uiPlace) repr�sentant la position de l'arc
	****** dans le tableau ARCSOMArcsArrivants
	****** Sortie : Pointeur vers l'objet CArc correspondant � l'arc arrivant
	****** � la position sp�cifi�e
	****** Pr�condition : Rien
	****** Postcondition : Rien
	*************************************************************************/
	CArc* ARCSOMGetArcsArrivants(unsigned int uiPlace); 


	/*************************************************************************
	Accesseur pour obtenir un arc partant sp�cifique
	/*************************************************************************
	****** Entr�e : Entier non sign� (uiPlace) repr�sentant la position de l'arc
	****** dans le tableau ARCSOMArcsPartants
	****** Sortie : Pointeur vers l'objet CArc correspondant � l'arc partant
	****** � la position sp�cifi�e
	****** Pr�condition : Rien
	****** Postcondition : Rien
	*************************************************************************/
	CArc* ARCSOMGetArcsPartants(unsigned int uiPlace);

public :
	
	/*************************************************************************
	Accesseur en �criture pour d�finir l'ID du sommet
	/*************************************************************************
	****** Entr�e : Entier non sign� (uiID) repr�sentant l'ID du sommet
	****** Sortie : Rien
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	void SOMSetID(unsigned int uiID);


	/*************************************************************************
	Obtenir le nombre d'arcs arrivants
	/*************************************************************************
	****** Entr�e : Rien
	****** Sortie : Entier non sign� repr�sentant le nombre d'arcs arrivants
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	unsigned int SOMGetNbsArcArrivants();


	/*************************************************************************
	Obtenir le nombre d'arcs partants
	/*************************************************************************
	****** Entr�e : Rien
	****** Sortie : Entier non sign� repr�sentant le nombre d'arcs partants
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	unsigned int SOMGetNbsArcPartants();


	/*************************************************************************
	Obtenir un tableau des destinations des arcs partants
	/*************************************************************************
	****** Entr�e : Rien
	****** Sortie : Objet CTableauDynamique<int> contenant les destinations
	****** des arcs partants
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	CTableauDynamique<int> TABSOMTableauDestinationArcsPartants();


	/*************************************************************************
	Obtenir un tableau des destinations des arcs arrivants
	/*************************************************************************
	****** Entr�e : Rien
	****** Sortie : Objet CTableauDynamique<int> contenant les destinations
	****** des arcs arrivants
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	CTableauDynamique<int> TABSOMTableauDestinationArcsArrivants();


	/*************************************************************************
	Ajoute un arc arrivant au sommet
	/*************************************************************************
	****** Entr�e : Entier non sign� repr�sentant l'ID de la destination de l'arc
	****** Sortie : Rien
	****** Pr�condition : Rien
	****** Postcondition : L'arc arrivant est ajout� au sommet s'il n'existe pas d�j�
	**************************************************************************/
	void SOMAjouterArcArrivant(unsigned int uiDestination);


	/*************************************************************************
	Supprime un arc partant du sommet
	/*************************************************************************
	****** Entr�e : Entier non sign� repr�sentant l'ID de la destination de l'arc
	****** Sortie : Rien
	****** Pr�condition : Rien
	****** Postcondition : L'arc partant est supprim� du sommet s'il existe
	**************************************************************************/
	void SOMAjouterArcPartant(unsigned int uiDestination);


	/*************************************************************************
	Ajoute un arc partant au sommet
	/*************************************************************************
	****** Entr�e : Entier non sign� repr�sentant l'ID de la destination de l'arc
	****** Sortie : Rien
	****** Pr�condition : Rien
	****** Postcondition : L'arc partant est ajout� au sommet s'il n'existe pas d�j�
	**************************************************************************/
	void SOMSupprimerArcArrivant(unsigned int uiDestination);


	/*************************************************************************
	Supprime un arc arrivant du sommet
	/*************************************************************************
	****** Entr�e : Entier non sign� repr�sentant l'ID de la destination de l'arc
	****** Sortie : Rien
	****** Pr�condition : Rien
	****** Postcondition : L'arc arrivant est supprim� du sommet s'il existe
	**************************************************************************/
	void SOMSupprimerArcPartant(unsigned int uiDestination);


	/*************************************************************************
	Trouve l'emplacement d'une destination d'un arc partant
	/*************************************************************************
	****** Entr�e : Entier non sign� repr�sentant l'ID de la destination de l'arc
	****** Sortie : Entier repr�sentant l'emplacement de la destination dans le tableau d'arcs partants (ou -1 si non trouv�)
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	int SOMTrouverDestinationArcPartant(unsigned int uiDestination);


	/*************************************************************************
	Trouve l'emplacement d'une destination d'un arc arrivant
	/*************************************************************************
	****** Entr�e : Entier non sign� repr�sentant l'ID de la destination de l'arc
	****** Sortie : Entier repr�sentant l'emplacement de la destination dans le tableau d'arcs arrivants (ou -1 si non trouv�)
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	int SOMTrouverDestinationArcArrivant(unsigned int uiDestination);


	/*************************************************************************
	Op�rateur d'affectation de la classe CSommet
	/*************************************************************************
	****** Entr�e : Objet CSommet (SOMArg) � copier
	****** Sortie : R�f�rence vers l'objet CSommet actuel apr�s la copie
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	CSommet& operator=(CSommet &SOMArg);
};
#endif
