#include <iostream>
#include "CParseur.h"
#include "CGraphe.h"
#include "CParseurGraphe.h"
#include "COperationGraphe.h"

int main(int argc, char *argv[])
{
	cout << "Debut" << endl;
	unsigned int uiBoucle = 0;
	
	CGraphe Graphe, Grapheinv;
	CParseurGraphe ObjetParseurOutils;
	COperationGraphe ObjetOperation;

	try {
		
		
		Graphe = (CGraphe&)(ObjetParseurOutils.GRAGenerer("TESTOT.txt"));
		Graphe.GRAAfficherGraphe();
		Grapheinv = (CGraphe&)(ObjetOperation.GRAOGRInverserGraphe(Graphe));
		Grapheinv.GRAAfficherGraphe();
		/*Graphe.GRASetOrientation(false);
		Graphe.GRAAfficherGraphe();
		Graphe.GRAChangeIdSommet(3, 7);
		Graphe.GRAAfficherGraphe();
		Graphe.GRASupprimerSommet(4);
		Graphe.GRAAfficherGraphe();
		Graphe.GRADeleteArc(1, 2);
		Graphe.GRAAfficherGraphe();
		cout << Graphe.GRAGetIdSommet(0) << endl;
		Graphe.GRAAjouterSommet(4);
		Graphe.GRAAjouterSommet(5);
		Graphe.GRAAjouterArc( 5 , 4);
		Graphe.GRAAfficherGraphe();
		Graphe.GRASupprimerSommet(4);
		Graphe.GRAAfficherGraphe();
		Grapheinv = (CGraphe&)(ObjetOperation.GRAOGRInverserGraphe(Graphe));
		Grapheinv.GRAAfficherGraphe();
		Graphe.GRASetOrientation(false);
		Graphe.GRAAfficherGraphe();
		Graphe.GRAChangeIdSommet(3, 7);
		Graphe.GRAAfficherGraphe();

		Graphe.GRAAjouterSommet(2);
		Graphe.GRAAjouterSommet(3);
		Graphe.GRAAjouterSommet(4);
		Graphe.GRAAjouterSommet(5);
		Graphe.GRAAfficherGraphe();
		Graphe.GRAChangeIdSommet(5, 56);
		Graphe.GRAAfficherGraphe();
		Graphe.GRASetOrientation(false);
		Graphe.GRAAfficherGraphe();
		Graphe.GRASupprimerSommet(2);
		Graphe.GRAAfficherGraphe();

		CTableauDynamique<int> tabtest;
		tabtest.TABAddElement(1);
		tabtest.TABAddElement(2);
		tabtest.TABAddElement(3);
		tabtest.TABAddElement(4);
		tabtest.TABAddElement(5);
		tabtest.TABAddElement(6);
		tabtest.TABAddElement(7);
		tabtest.TABAddElement(8);
		cout << *tabtest.TABGetElement(0) << endl;
		CTableauDynamique<int> tabtest2 = tabtest;
		cout << *tabtest2.TABGetElement(5) << endl;
		tabtest2 = (CTableauDynamique<int>&)(Graphe.TABGRATableauDestinationArcsArrivants(1));
		cout << *tabtest2.TABGetElement(0) << endl;*/
			
	}
	catch (CException EXCLevee) {
		cout << "Exception " << EXCLevee.EXClire_valeur() << " a ete levee" << endl;
		EXCLevee.EXCAfficherErreur();
	}

	
	return 0;
	
}
