#pragma once
#ifndef CTABLEAUDYNAMIQUE_H
#define CTABLEAUDYNAMIQUE_H
#include "CException.h"

#define EXCInvalidTailleSaisie 1
#define EXCInvalidEmplacementSaisie 2

template<class T > class CTableauDynamique
{
private:
	T **TABTableau;
	unsigned int uiTABTailleTableau;

public:

	/*************************************************************************
	Constructeur par d�faut de la classe CTableauDynamique
	/*************************************************************************
	****** Entr�e : Rien
	****** Sortie : Rien
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	CTableauDynamique();


	/*************************************************************************
	Constructeur de copie de la classe CTableauDynamique
	/*************************************************************************
	****** Entr�e : Objet CTableauDynamique<T> (TabArg) � copier
	****** Sortie : Rien
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	CTableauDynamique(CTableauDynamique<T> &TabArg);


	/*************************************************************************
	Destructeur de la classe CTableauDynamique
	/*************************************************************************
	****** Entr�e : Rien
	****** Sortie : Rien
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	~CTableauDynamique();


	/*************************************************************************
	Accesseur pour obtenir la taille du tableau
	/*************************************************************************
	****** Entr�e : Rien
	****** Sortie : Entier non sign� repr�sentant la taille du tableau
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	unsigned int TABGetTaille();


	/*************************************************************************
	Ajoute un �l�ment au tableau
	/*************************************************************************
	****** Entr�e : T non sign� repr�sentant l'�lement � ajouter
	****** Sortie : Rien
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	void TABAddElement(T* Element);


	/*************************************************************************
	Ajoute un �l�ment du tableau
	/*************************************************************************
	****** Entr�e : T non sign� repr�sentant l'�lement � ajouter
	****** Sortie : Rien
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	void TABAddElement(const T& Element);


	/*************************************************************************
	Supprime un �l�ment du tableau
	/*************************************************************************
	****** Entr�e : Entier non sign� repr�sentant l'emplacement � supprimer
	****** Sortie : Rien
	****** Pr�condition :	uiEmplacement <= 0
	******					uiEmplacement > uiTABTailleTableau
	****** Postcondition : Rien
	**************************************************************************/
	void TABDeleteElement(unsigned int uiEmplacement);


	/*************************************************************************
	Obtenir le nombre d'�l�ment du tableau
	/*************************************************************************
	****** Entr�e : Entier non sign� repr�sentant l'emplacement � chercher
	****** Sortie : Emplcament du tableau recherch�
	****** Pr�condition :	uiEmplacement <= 0
	******					uiEmplacement > uiTABTailleTableau
	****** Postcondition : Rien
	**************************************************************************/
	T* TABGetElement(unsigned int uiEmplacement);


	/*************************************************************************
	Op�rateur d'affectation de la classe CTableauDynamique
	/*************************************************************************
	****** Entr�e : Objet CTableauDynamique<T> (TABArg) � copier
	****** Sortie : R�f�rence vers l'objet CTableauDynamique actuel apr�s la copie
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	CTableauDynamique<T>& operator=(CTableauDynamique<T> &TABArg);

};


#include "CTableauDynamique.hpp"
#endif