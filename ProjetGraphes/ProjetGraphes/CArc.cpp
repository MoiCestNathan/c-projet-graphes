#include "CArc.h"

/*************************************************************************
Constructeur par d�faut de la classe CArc
/*************************************************************************
****** Entr�e : Rien
****** Sortie : Rien
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
CArc::CArc()
{
}

/*************************************************************************
Constructeur de copie de la classe CArc
/*************************************************************************
****** Entr�e : Objet CArc (ARCArg) � copier
****** Sortie : Rien
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
CArc::CArc(CArc & ARCArg)
{
	uiARCDestination = ARCArg.uiARCDestination;
}

/*************************************************************************
Constructeur de la classe CArc avec destination sp�cifi�e
/*************************************************************************
****** Entr�e : Entier non sign� (uiDestination) repr�sentant la destination de l'arc
****** Sortie : Rien
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
CArc::CArc(unsigned int uiDestination)
{
	uiARCDestination = uiDestination;
}

/*************************************************************************
Destructeur de la classe CArc
/*************************************************************************
****** Entr�e : Rien
****** Sortie : Rien
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
CArc::~CArc()
{

}

/*************************************************************************
Accesseur en lecture pour obtenir la destination de l'arc
/*************************************************************************
****** Entr�e : Rien
****** Sortie : Entier non sign� repr�sentant la destination de l'arc
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
unsigned int CArc::ARCgetDestination()
{
	return uiARCDestination;
}

/*************************************************************************
Accesseur en �criture pour d�finir la destination de l'arc
/*************************************************************************
****** Entr�e : Entier non sign� (uiDestination) repr�sentant la nouvelle destination de l'arc
****** Sortie : Rien
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
void CArc::ARCsetDestination(unsigned int uiDestination)
{
	uiARCDestination = uiDestination;
}

/*************************************************************************
Op�rateur d'affectation de la classe CArc
/*************************************************************************
****** Entr�e : Objet CArc (ArcArg) � affecter
****** Sortie : R�f�rence vers l'objet CArc modifi�
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
CArc& CArc::operator=(CArc & ArcArg)
{
	uiARCDestination = ArcArg.uiARCDestination;
	return *this;
}

