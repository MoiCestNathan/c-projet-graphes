#pragma once
#ifndef CPARSEURGRAPHE_H
#define CPARSEURGRAPHE_H
#include "CGraphe.h"

class CParseurGraphe
{
public:

	/*************************************************************************
	M�thode pour g�n�rer le Graphe
	/*************************************************************************
	****** Entr�e : Chemin du fichier
	****** Sortie : Objet CGraphe
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	CGraphe GRAGenerer(const char * CheminFichier);
};

#endif