#pragma once
#ifndef COPERATIONGRAPHE_H
#define COPERATIONGRAPHE_H
#include "CGraphe.h"


class COperationGraphe
{
public:
	/*************************************************************************
	M�thode pour inverser le Graphe
	/*************************************************************************
	****** Entr�e : Objet CGraphe
	****** Sortie : Objet CGraphe
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	CGraphe GRAOGRInverserGraphe(CGraphe GRAArg);
};

#endif