﻿#include <iostream>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <stdio.h>
#include <string.h>
#include "CParseur.h"

using namespace std;


/*************************************************************************
Parseur pour les parties simples (NBSommets et NBArcs)
/*************************************************************************
****** Entrée : Chemin du fichier à parser, chaine de caractères à chercher
****** Sortie : Reourne la chaine de caractère recherché
****** Précondition :		On doit pouvoir ouvrir le fichier : ifsFichier == 0
******						La chaine de caractères "pcChaineCarac" doit être présent dans le fichier
******						La partie après le égale ne doitpas être nul : pcPartieDroite != nullptr
******						Doit être un nombre à droite du égal : *pcEnd != '\0'
****** Postcondition : Rien
**************************************************************************/
char * CParseur::PARParserPartieSimple(const char * CheminFichier, char* pcChaineCarac)
{
	ifstream ifsFichier(CheminFichier);

	if (!ifsFichier) {
		CException EXCLevee(EXCProblemeOuverture, "Erreur lors de l'ouverture du fichier");
		throw(EXCLevee);
	}

	char cSeparateurEgal[] = "=";
	char cSeparateurEspaceEtTabulation[] = " 	";
	char cLigne[256];

	char* pcEnd;
	char* pcPartieGauche = nullptr;
	char* pcPartieDroite = nullptr;
	char* pcTemp = nullptr;

	unsigned int uiBoucle = 0;

	while (ifsFichier.getline(cLigne, sizeof(cLigne))) {
		
		for (uiBoucle = 0; uiBoucle < 256; uiBoucle++) {
			cLigne[uiBoucle] = tolower(cLigne[uiBoucle]);
			if (cLigne[uiBoucle] == '\0') {
				uiBoucle = 256;
			}
		}
		
		pcPartieGauche = strtok_s(cLigne, cSeparateurEgal, &pcTemp);

		if (pcPartieGauche != nullptr) {

			if (strstr(pcPartieGauche, pcChaineCarac) != nullptr) {

				pcPartieDroite = strtok_s(nullptr, cSeparateurEspaceEtTabulation, &pcTemp);

				if (pcPartieDroite != nullptr) {

					strtol(pcPartieDroite, &pcEnd, 10);

					if (*pcEnd == '\0') {
						return pcPartieDroite;
					}
					else {
						cout << "-- " << pcChaineCarac << " --" << endl;
						CException EXCLevee(EXCErreurNombre, "Erreur : Le nombre apres l'egalite est invalide");
						throw(EXCLevee);
					}
				}
				else {
					cout << "--- " << pcChaineCarac << " ---" << endl;
					CException EXCLevee(EXCErreurPartieDroite, "Erreur lors de la lecture du chiffre après le egal");
					throw(EXCLevee);
				}
			}
		}
	}
	ifsFichier.close();

	cout << "----- " << pcChaineCarac << " -----" << endl;
	CException EXCLevee(EXCChaineCaracIntrouvable, "Chaine de caracteres non trouvee dans le fichier");
	throw(EXCLevee);
}


/*************************************************************************
Parseur pour les parties complexes (Sommets -> Numero et Arcs -> Debut | Fin )
/*************************************************************************
****** Entrée : Chemin du fichier à parser, première chaine de caractères à chercher, nombre de sommets ou d'arcs, deuxième chaine de caractères à chercher
****** Sortie : Reourne la chaine de caractère recherché
****** Précondition :		On doit pouvoir ouvrir le fichier : ifsFichier == 0
******						Le fichier doit contenir le bombre attendu d'arcs ou de sommets
******						La chaine de caractères "pcChaineCarac" doit être présent dans le fichier
******						La partie après le égale ne doitpas être nul : pcPartieDroite != nullptr
******						Doit être un nombre à droite du égal : *pcEnd != '\0'
****** Postcondition : Rien
**************************************************************************/
char ** CParseur::PARParserPartieComplexe(const char* pcCheminFichier, char* pcChaineCarac, unsigned int uiNbrLignes, char* pcChaineCarac2)
{
	ifstream ifsFichier(pcCheminFichier);

	if (!ifsFichier) {
		CException EXCLevee(EXCProblemeOuverture, "Erreur lors de l'ouverture du fichier");
		throw(EXCLevee);
	}

	char** ppcResultat = new char*[uiNbrLignes];

	char* pcEnd;
	char* pcPartieGauche = nullptr;
	char* pcPartieDroite = nullptr;
	char* cTemp = nullptr;

	char cSeparateurEgal[] = "=";
	char cSeparateurEspaceEtTabulation[] = " 	,";
	char cLigne[256];

	bool bLigneVide = true;

	unsigned int uiBoucle = 0 , uiBoucle2 = 0 , uiCurseur = 0;


	while (ifsFichier.getline(cLigne, sizeof(cLigne))) {
		
		for (uiBoucle2 = 0; uiBoucle2 < 256; uiBoucle2++) {
			cLigne[uiBoucle2] = tolower(cLigne[uiBoucle2]);
			if (cLigne[uiBoucle2] == '\0') {
				uiBoucle2 = 256;
			}
		}
		pcPartieGauche = strtok_s(cLigne, cSeparateurEgal, &cTemp);

		if (pcPartieGauche != nullptr && strstr(pcPartieGauche, pcChaineCarac) != nullptr && strstr(cTemp, "[")) {
			for (uiBoucle = 0; uiBoucle < uiNbrLignes+1 ; uiBoucle++) {
				
				bLigneVide = true;

				ifsFichier.getline(cLigne, sizeof(cLigne));
				
				for (uiBoucle2 = 0; uiBoucle2 < 256; uiBoucle2++) {
					cLigne[uiBoucle2] = tolower(cLigne[uiBoucle2]);
					if(cLigne[uiBoucle2] != ' ' && cLigne[uiBoucle2] != '\t' && cLigne[uiBoucle2] != '\0' ) {
						bLigneVide = false;
					}
					if (cLigne[uiBoucle2] == '\0') {
						uiBoucle2 = 256;
					}
				}

				if (bLigneVide == false) {
					if (uiBoucle == uiNbrLignes)
					{
						for (uiBoucle2 = 0; uiBoucle2 < 256; uiBoucle2++) {
							if (cLigne[uiBoucle2] != ' ' && cLigne[uiBoucle2] != '\t' ) {
								cLigne[uiCurseur] = cLigne[uiBoucle2];
								uiCurseur++;
							}
							if (cLigne[uiBoucle2] == '\0') {
								
								cLigne[uiCurseur] = '\0';
								uiBoucle2 = 256;
							}
							else {
								cLigne[uiBoucle2] = ' ';
							}
							
						}
						if (strstr(cLigne, "]") == nullptr) {
							CException EXCLevee(EXCIncoherenceNbrAttendu, "Incoherence entre le nombre de Sommet ou Arc annonces et le nombre de Sommet ou Arc reel ou oublie crochet fermant");
							throw(EXCLevee);
						}
					}


					pcPartieGauche = strtok_s(cLigne, cSeparateurEgal, &cTemp);

					if (strstr(pcPartieGauche, pcChaineCarac2) == nullptr && strstr(cTemp, pcChaineCarac2) != nullptr) {
						pcPartieGauche = strtok_s(cTemp, cSeparateurEgal, &pcPartieDroite);

						if (pcPartieDroite != nullptr) {

							strtol(pcPartieDroite, &pcEnd, 10);

							if (*pcEnd == '\0') {
								ppcResultat[uiBoucle] = _strdup(pcPartieDroite);
							}
							else {
								cout << "-- " << pcChaineCarac << " --" << endl;
								CException EXCLevee(EXCErreurNombre, "Erreur : Le nombre apres l'egalite est invalide");
								throw(EXCLevee);
							}


						}
						else
						{
							cout << "--- " << pcChaineCarac << " ---" << endl;
							CException EXCLevee(EXCErreurPartieDroite, "Erreur lors de la lecture du chiffre après le egal");
							throw(EXCLevee);
						}
					}
					else if (pcPartieGauche != nullptr && strstr(pcPartieGauche, pcChaineCarac2)) {

						pcPartieDroite = strtok_s(nullptr, cSeparateurEspaceEtTabulation, &cTemp);

						if (pcPartieDroite != nullptr) {

							strtol(pcPartieDroite, &pcEnd, 10);

							if (*pcEnd == '\0') {
								ppcResultat[uiBoucle] = _strdup(pcPartieDroite);
							}
							else {
								cout << "-- " << pcChaineCarac << " --" << endl;
								CException EXCLevee(EXCErreurNombre, "Erreur : Le nombre apres l'egalite est invalide");
								throw(EXCLevee);
							}
						}
						else
						{
							cout << "--- " << pcChaineCarac << " ---" << endl;
							CException EXCLevee(EXCErreurPartieDroite, "Erreur lors de la lecture du chiffre après le egal");
							throw(EXCLevee);
						}
					}

					/*if (uiBoucle == uiNbrLignes ) {
						if (strstr(cLigne, "]") == nullptr) {
							CException EXCLevee(EXCAbsenceDeCrochetFermant, "Il manque le crochet fermant");
							throw(EXCLevee);
						}

					}*/

					if ((uiBoucle == uiNbrLignes && *pcPartieDroite != '\0' ) || (uiBoucle != uiNbrLignes && *pcPartieDroite == '\0' ))
					{
						CException EXCLevee(EXCIncoherenceNbrAttendu, "Incoherence caca entre le nombre de Sommet ou Arc annonces et le nombre de Sommet ou Arc reel");
						throw(EXCLevee);
					}
				}
				else {
					uiBoucle--;
				}
			}



			ifsFichier.close();

			return ppcResultat;

			if (ppcResultat != nullptr) {
				for (unsigned int i = 0; i < uiNbrLignes; ++i) {
					delete[] ppcResultat[i];
				}
				delete[] ppcResultat;
			}

			delete[] pcPartieGauche;
			delete[] pcPartieDroite;
			delete[] cTemp;
		}
	}
	cout << "----- " << pcChaineCarac << " -----" << endl;
	CException EXCLevee(EXCChaineCaracIntrouvable, "Chaine de caracteres non trouvee dans le fichier");
	throw(EXCLevee);
}