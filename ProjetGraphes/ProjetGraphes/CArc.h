#pragma once
#ifndef CARC_H
#define CARC_H

#include "stdlib.h"


class CArc
{
private:

	unsigned int uiARCDestination; // Numero du sommet de destination 

public:

	/*************************************************************************
	Constructeur par d�faut de la classe CArc
	/*************************************************************************
	****** Entr�e : Rien
	****** Sortie : Rien
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	CArc();

	/*************************************************************************
	Constructeur de copie de la classe CArc
	/*************************************************************************
	****** Entr�e : Objet CArc (ARCArg) � copier
	****** Sortie : Rien
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	CArc(CArc & ARCArg);

	/*************************************************************************
	Constructeur de la classe CArc avec destination sp�cifi�e
	/*************************************************************************
	****** Entr�e : Entier non sign� (uiDestination) repr�sentant la destination de l'arc
	****** Sortie : Rien
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	CArc(unsigned int uiDestination);

	/*************************************************************************
	Destructeur de la classe CArc
	/*************************************************************************
	****** Entr�e : Rien
	****** Sortie : Rien
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	~CArc();

	/*************************************************************************
	Accesseur en lecture pour obtenir la destination de l'arc
	/*************************************************************************
	****** Entr�e : Rien
	****** Sortie : Entier non sign� repr�sentant la destination de l'arc
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	unsigned int ARCgetDestination();

	/*************************************************************************
	Accesseur en �criture pour d�finir la destination de l'arc
	/*************************************************************************
	****** Entr�e : Entier non sign� (uiDestination) repr�sentant la nouvelle destination de l'arc
	****** Sortie : Rien
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	void ARCsetDestination(unsigned int uiDestination);

	/*************************************************************************
	Op�rateur d'affectation de la classe CArc
	/*************************************************************************
	****** Entr�e : Objet CArc (ArcArg) � affecter
	****** Sortie : R�f�rence vers l'objet CArc modifi�
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	CArc& operator=(CArc & ArcArg);
};

#endif
