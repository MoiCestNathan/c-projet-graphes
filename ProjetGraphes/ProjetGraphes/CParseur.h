#pragma once
#ifndef CPARSEUR_H
#define CPARSEUR_H

#include <iostream>
#include <fstream>
#include "CException.h"
#include "CGraphe.h"

#define EXCProblemeOuverture		30
#define EXCFormat					31

#define EXCErreurPartieDroite		32
#define EXCErreurNombre				33
#define EXCChaineCaracIntrouvable	34

#define EXCIncoherenceNbrAttendu   35
#define EXCAbsenceDeCrochetFermant  36

using namespace std;

class CParseur
{
public:

	/*************************************************************************
	Parseur pour les parties simples (NBSommets et NBArcs)
	/*************************************************************************
	****** Entr�e : Chemin du fichier � parser, chaine de caract�res � chercher
	****** Sortie : Reourne la chaine de caract�re recherch�
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	char* PARParserPartieSimple(const char* pcCheminFichier, char* pcChaineCarac);


	/*************************************************************************
	Parseur pour les parties complexes (Sommets -> Numero et Arcs -> Debut | Fin )
	/*************************************************************************
	****** Entr�e : Chemin du fichier � parser, premi�re chaine de caract�res � chercher, nombre de sommets ou d'arcs, deuxi�me chaine de caract�res � chercher
	****** Sortie : Reourne la chaine de caract�re recherch�
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	char **PARParserPartieComplexe(const char* pcCheminFichier, char* pcChaineCarac, unsigned int uiNbrLignes, char* pcChaineCarac2);
};

#endif