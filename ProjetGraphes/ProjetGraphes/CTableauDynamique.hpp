#ifndef CTABLEAUDYNAMIQUE_HPP
#define CTABLEAUDYNAMIQUE_HPP



#include "CTableauDynamique.h"
#include <stdio.h>
#include <cstdlib>


/*************************************************************************
Constructeur par d�faut de la classe CTableauDynamique
/*************************************************************************
****** Entr�e : Rien
****** Sortie : Rien
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
template<class T>
CTableauDynamique<T>::CTableauDynamique() {
	
	TABTableau = nullptr ;
	uiTABTailleTableau = 0;

}


/*************************************************************************
Constructeur de copie de la classe CTableauDynamique
/*************************************************************************
****** Entr�e : Objet CTableauDynamique<T> (TabArg) � copier
****** Sortie : Rien
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
template<class T>
CTableauDynamique<T>::CTableauDynamique(CTableauDynamique<T> &TabArg) {
	
	unsigned int uiBoucle = 0;
	
	uiTABTailleTableau = TabArg.uiTABTailleTableau;

	if (uiTABTailleTableau == 0) {
		TABTableau = nullptr;
	}
	else {
		TABTableau = (T**)(malloc(sizeof(T*) * uiTABTailleTableau));

		for (uiBoucle = 0; uiBoucle < uiTABTailleTableau; uiBoucle++) {
			TABTableau[uiBoucle] = new T(*TabArg.TABTableau[uiBoucle]);
		}
	}
}


/*************************************************************************
Destructeur de la classe CTableauDynamique
/*************************************************************************
****** Entr�e : Rien
****** Sortie : Rien
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
template<class T>
CTableauDynamique<T>::~CTableauDynamique() {
	
	unsigned int uiBoucle;

	if (TABTableau != nullptr) {
		for (uiBoucle = 0; uiBoucle < uiTABTailleTableau; uiBoucle++) {
			free(TABTableau[uiBoucle]);
		}	
		free(TABTableau);
	}
}


/*************************************************************************
Accesseur pour obtenir la taille du tableau
/*************************************************************************
****** Entr�e : Rien
****** Sortie : Entier non sign� repr�sentant la taille du tableau 
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
template<class T>
unsigned int CTableauDynamique<T>::TABGetTaille() {
	return uiTABTailleTableau;
}


/*************************************************************************
Ajoute un �l�ment au tableau 
/*************************************************************************
****** Entr�e : T non sign� repr�sentant l'�lement � ajouter
****** Sortie : Rien
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
template<typename T>
void CTableauDynamique<T>::TABAddElement(T* Element) {
	
	if (uiTABTailleTableau == 0) {
		TABTableau = (T**)(malloc( sizeof(T*) * 1 )); 
	}
	else {
		TABTableau = (T**)(realloc(TABTableau, (uiTABTailleTableau + 1) * sizeof(T*)));
	}
	TABTableau[uiTABTailleTableau] = new T(*Element); 
	uiTABTailleTableau++;	
}


/*************************************************************************
Ajoute un �l�ment du tableau
/*************************************************************************
****** Entr�e : T non sign� repr�sentant l'�lement � ajouter
****** Sortie : Rien
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
template<typename T>
void CTableauDynamique<T>::TABAddElement(const T& Element) {

	if (uiTABTailleTableau == 0) {
		TABTableau = (T**)(malloc(sizeof(T*) * 1));
	}
	else {
		TABTableau = (T**)(realloc(TABTableau, (uiTABTailleTableau + 1) * sizeof(T*)));
	}
	TABTableau[uiTABTailleTableau] = new T(Element);
	uiTABTailleTableau++;
}


/*************************************************************************
Supprime un �l�ment du tableau
/*************************************************************************
****** Entr�e : Entier non sign� repr�sentant l'emplacement � supprimer
****** Sortie : Rien
****** Pr�condition :	uiEmplacement <= 0 
******					uiEmplacement > uiTABTailleTableau
****** Postcondition : Rien
**************************************************************************/
template<class T>
void CTableauDynamique<T>::TABDeleteElement(unsigned int uiEmplacement) {
	
	unsigned int uiBoucle = 0;
	
	if (uiEmplacement >= 0 && uiEmplacement < uiTABTailleTableau) {
		free(TABTableau[uiEmplacement]);
		TABTableau[uiEmplacement] = TABTableau[uiTABTailleTableau - 1];
		uiTABTailleTableau--;
		TABTableau = (T**)(realloc(TABTableau, (uiTABTailleTableau) * sizeof(T*)));
	}
	else {
		CException EXCLevee(EXCInvalidEmplacementSaisie, "L'emplacement demandee n'est pas dans le tableau");
		throw(EXCLevee);
	}
}


/*************************************************************************
Obtenir le nombre d'�l�ment du tableau
/*************************************************************************
****** Entr�e : Entier non sign� repr�sentant l'emplacement � chercher
****** Sortie : Emplcament du tableau recherch�
****** Pr�condition :	uiEmplacement <= 0 
******					uiEmplacement > uiTABTailleTableau
****** Postcondition : Rien
**************************************************************************/
template<typename T>
T* CTableauDynamique<T>::TABGetElement(unsigned int uiEmplacement)
{
	if (uiEmplacement >= 0 && uiEmplacement < uiTABTailleTableau) {
		return TABTableau[uiEmplacement];
	}
	else {
		CException EXCLevee(EXCInvalidEmplacementSaisie, "L'emplacement demandee n'est pas dans le tableau");
		throw(EXCLevee);
	}
}

/*************************************************************************
Op�rateur d'affectation de la classe CTableauDynamique
/*************************************************************************
****** Entr�e : Objet CTableauDynamique<T> (TABArg) � copier
****** Sortie : R�f�rence vers l'objet CTableauDynamique actuel apr�s la copie
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
template<class T>
CTableauDynamique<T>& CTableauDynamique<T>::operator=(CTableauDynamique<T> &TABArg) {
	unsigned int uiBoucle = 0;
	if (TABTableau != nullptr) {
		
		for (uiBoucle = 0; uiBoucle < uiTABTailleTableau; uiBoucle++) {			
			delete TABTableau[uiBoucle];
		}		
		free(TABTableau);
	}
	if (TABArg.uiTABTailleTableau == 0) {		
		TABTableau = nullptr;
	}
	else {
		uiTABTailleTableau = TABArg.uiTABTailleTableau;
		TABTableau = (T**)malloc( sizeof(T*) * TABArg.uiTABTailleTableau );
		for (uiBoucle = 0; uiBoucle < uiTABTailleTableau; uiBoucle++) {
			TABTableau[uiBoucle] = new T(*TABArg.TABGetElement(uiBoucle));
		}
	}	
	return *this;
}

#endif