#include "COperationGraphe.h"

/*************************************************************************
M�thode pour inverser le Graphe
/*************************************************************************
****** Entr�e : Objet CGraphe
****** Sortie : Objet CGraphe
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
CGraphe COperationGraphe::GRAOGRInverserGraphe(CGraphe GRAArg)
{
	unsigned int uiBoucle = 0, uiBoucle2 = 0;
	CGraphe GRATemp;
	CTableauDynamique<int> TableauDestinationArcsPartants;
	for (uiBoucle = 0; uiBoucle < GRAArg.GRAGetNbSommets() ; uiBoucle++) {
		GRATemp.GRAAjouterSommet(GRAArg.GRAGetIdSommet(uiBoucle));
	}
	for (uiBoucle = 0; uiBoucle < GRAArg.GRAGetNbSommets(); uiBoucle++) {
		int iIdSommet = GRATemp.GRAGetIdSommet(uiBoucle);
		if (GRAArg.TABGRATableauDestinationArcsPartants(iIdSommet).TABGetTaille() != 0) {
			TableauDestinationArcsPartants = (CTableauDynamique<int>&)GRAArg.TABGRATableauDestinationArcsPartants(iIdSommet);
			int iTaille = TableauDestinationArcsPartants.TABGetTaille();
			for (uiBoucle2 = 0; uiBoucle2 < iTaille; uiBoucle2++) {

				GRATemp.GRAAjouterArc(*TableauDestinationArcsPartants.TABGetElement(uiBoucle2), GRATemp.GRAGetIdSommet(uiBoucle));
			}
		}
	}
	return GRATemp;
}