#pragma once
#ifndef CGRAPHE_H
#define CGRAPHE_H

#define EXCInvalidSommetPosition	20
#define EXCInvalidSommetId			21
#define EXCSommetNotFound			22
#define EXCIdSommetNotExist         23
#define EXCIDSommetDepartNonExistant 24
#define EXCIDSommetArriveNonExistant 25
#define EXCIDSommetNonExistant 26


#include "CSommet.h"
#include "CTableauDynamique.h"
#include "CParseur.h"

using namespace std;

class CGraphe
{
private:

	CTableauDynamique<CSommet> TABGRASommets;

	bool bOriente;

public:

	/*************************************************************************
	Constructeur par d�faut de la classe CGraphe
	/*************************************************************************
	****** Entr�e : Rien
	****** Sortie : Rien
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	CGraphe();


	/*************************************************************************
	Constructeur de la classe CGraphe avec param�tre
	/*************************************************************************
	****** Entr�e : bool�en bOrientation pour sp�cifier l'orientation du graphe
	****** Sortie : Rien
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	CGraphe(bool bOrientation);


	/*************************************************************************
	Constructeur de copie de la classe CGraphe
	/*************************************************************************
	****** Entr�e : Objet CGraphe (GRAArg) � copier
	****** Sortie : Rien
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	CGraphe(CGraphe &GRAArg);


	/*************************************************************************
	Destructeur de la classe CGraphe
	/*************************************************************************
	****** Entr�e : Rien
	****** Sortie : Rien
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	~CGraphe();


	/*************************************************************************
	Accesseur pour obtenir l'orientation du graphe
	/*************************************************************************
	****** Entr�e : Rien
	****** Sortie : Bool�en repr�sentant l'orientation du graphe
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	bool GRAGetOrienation();


	/*************************************************************************
	Accesseur en �criture pour d�finir l'orientation du graphe
	/*************************************************************************
	****** Entr�e : Bool�en repr�sentant la nouvelle orientation du graphe
	****** Sortie : Rien
	****** Pr�condition : Rien
	****** Postcondition : Les arcs du graphe peuvent �tre modifi�s selon la nouvelle orientation
	**************************************************************************/
	void GRASetOrientation(bool bChangement);


	/*************************************************************************
	Accesseur pour obtenir le nombre de sommets du graphe
	/*************************************************************************
	****** Entr�e : Rien
	****** Sortie : Entier non sign� repr�sentant le nombre de sommets
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	unsigned int GRAGetNbSommets() ;


private :

	/*************************************************************************
	Accesseur pour obtenir un sommet du graphe
	/*************************************************************************
	****** Entr�e : Entier non sign� repr�sentant la position du sommet dans le tableau
	****** Sortie : Pointeur vers l'objet CSommet correspondant au sommet demand�
	****** Pr�condition : Rien
	****** Postcondition : Rien
	*************************************************************************/
	CSommet* GRAGetSommet(unsigned int uiPos);

public :

	/*************************************************************************
	Accesseur pour obtenir l'ID d'un sommet du graphe
	/*************************************************************************
	****** Entr�e : Position du sommet dans le tableau de sommets (uiPos)
	****** Sortie : Entier repr�sentant l'ID du sommet
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	int GRAGetIdSommet(unsigned int uiPos);


	/*************************************************************************
	Changer l'ID d'un sommet du graphe
	/*************************************************************************
	****** Entr�e : ID actuel du sommet (uiIdSommet), nouvel ID du sommet (uiNouvelId)
	****** Sortie : Rien
	****** Pr�condition : iEmplacement != -1 | Le sommet doit �tre pr�sent
	****** Postcondition : Rien
	**************************************************************************/
	void GRAChangeIdSommet(unsigned int uiIdSommet, unsigned int uiNouvelId);


	/*************************************************************************
	Ajouter un sommet au graphe
	/*************************************************************************
	****** Entr�e : ID du sommet � ajouter (uiIDSommet)
	****** Sortie : Rien
	****** Pr�condition : uiIDSommet > 0
	****** Postcondition : Rien
	*************************************************************************/
	void GRAAjouterSommet(unsigned int uiIDSommet);


	/*************************************************************************
	Supprimer un sommet du graphe
	/*************************************************************************
	****** Entr�e : ID du sommet � supprimer (uiIdSommetASupprimer)
	****** Sortie : Rien
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	void GRASupprimerSommet(unsigned int uiIdSommetASupprimer);


	/*************************************************************************
	Ajouter un arc entre deux sommets du graphe
	/*************************************************************************
	****** Entr�e : ID du sommet de d�part (Depart), ID du sommet d'arriv�e (Arrive)
	****** Sortie : Rien
	****** Pr�condition :	iEmplacementArrive != -1 
	******					iEmplacementDepart != -1
	****** Postcondition : Rien
	**************************************************************************/
	void GRAAjouterArc(unsigned int Depart, unsigned int Arrive);


	/*************************************************************************
	Supprimer un arc entre deux sommets du graphe
	/*************************************************************************
	****** Entr�e : ID du sommet de d�part (Depart), ID du sommet d'arriv�e (Arrive)
	****** Sortie : Rien
	****** Pr�condition :	iEmplacementArrive != -1 
	******					iEmplacementDepart != -1
	****** Postcondition : Rien
	**************************************************************************/
	void GRADeleteArc(unsigned int Depart, unsigned int Arrive);


	/*************************************************************************
	Afficher le graphe
	**************************************************************************
	****** Entr�e : Rien
	****** Sortie : Rien
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	void GRAAfficherGraphe();


	/*************************************************************************
	Trouver l'ID d'un sommet
	**************************************************************************
	****** Entr�e : Entier non sign� repr�sentant l'ID du sommet � trouver
	****** Sortie : Entier non sign� repr�sentant l'indice du sommet dans le tableau
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**********************************************************************/
	unsigned int GRATrouverId(unsigned int uiIdATrouver);


	/*************************************************************************
	Obtenir un tableau des IDs des sommets
	**************************************************************************
	****** Entr�e : Rien
	****** Sortie : Objet CTableauDynamique<int> contenant les IDs des sommets
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	CTableauDynamique<int> TABGRATableauIDSommets();


	/*************************************************************************
	Obtenir un tableau des destinations des arcs partants d'un sommet
	**************************************************************************
	****** Entr�e : Entier non sign� repr�sentant l'ID du sommet
	****** Sortie : Objet CTableauDynamique<int> contenant les destinations des arcs partants
	****** Pr�condition : iEmplacement != -1 | Le sommet doit �tre pr�sent
	****** Postcondition : Rien
	**************************************************************************/
	CTableauDynamique<int> TABGRATableauDestinationArcsPartants(unsigned int uiIdSommet);


	/*************************************************************************
	Obtenir un tableau des destinations des arcs arrivants � un sommet
	**************************************************************************
	****** Entr�e : Entier non sign� repr�sentant l'ID du sommet
	****** Sortie : Objet CTableauDynamique<int> contenant les destinations des arcs arrivants
	****** Pr�condition : iEmplacement != -1 | Le sommet doit �tre pr�sent
	****** Postcondition : Rien
	**************************************************************************/
	CTableauDynamique<int> TABGRATableauDestinationArcsArrivants(unsigned int uiIdSommet);


	/*************************************************************************
	Op�rateur d'affectation de la classe CGraphe
	**************************************************************************
	****** M�thode utilis�e : Op�rateur d'affectation
	****** Entr�e : R�f�rence vers un objet CGraphe (GRAArg) � copier
	****** Sortie : R�f�rence vers l'objet CGraphe actuel apr�s la copie
	****** Pr�condition : Rien
	****** Postcondition : Rien
	**************************************************************************/
	CGraphe& operator=(CGraphe &GRAArg);

};

#endif
