#include "CException.h"


/*************************************************************************
				Constructeur par defaut de la classe CException
**************************************************************************
******		Entree :		Rien
******		Sortie :		Rien
******		Precondition :	Rien
******		Postcondition : Rien
**************************************************************************/
CException::CException()
{
	uiEXCvaleur = EXCErreurParDefaut;
	cEXCErreur[1000] = NULL;
}

/*************************************************************************
				Constructeur par defaut de la classe CException
**************************************************************************
******		Entree :		uiEXobjet -> Le numero de l'erreur
******						cEXCobjet -> Chaine de caractere pour stocker le message d'erreur asscocie a l'erreur
******		Sortie :		Rien
******		Precondition :	Rien
******		Postcondition : Rien
**************************************************************************/
CException::CException(unsigned int uiEXCobjet, const char cEXCobjet[1000])
{
	uiEXCvaleur = uiEXCobjet;
	strcpy_s(cEXCErreur, 1000, cEXCobjet);
}

/*************************************************************************
				Destructeur par defaut de la classe CException
**************************************************************************
******		Entree :		Rien
******		Sortie :		Rien
******		Precondition :	Rien
******		Postcondition : Rien
**************************************************************************/
CException::~CException()
{
	uiEXCvaleur = EXCErreurParDefaut;
}

/*************************************************************************
				Accesseur en lecture du code d'erreur
**************************************************************************
******		Entree :		Rien
******		Sortie :		Code d'erreur
******		Precondition :  Rien
******		Postcondition : Rien
**************************************************************************/
int CException::EXClire_valeur()
{
	return (uiEXCvaleur);
}

/*************************************************************************
				Afficher l'erreur
**************************************************************************
******		Entree :		Rien
******		Sortie :		Rien
******		Precondition :  Rien
******		Postcondition : Rien
**************************************************************************/
void CException::EXCAfficherErreur()
{
	std::cout << cEXCErreur << std::endl;
}
