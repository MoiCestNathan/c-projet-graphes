#include "CSommet.h"

/*************************************************************************
Constructeur par d�faut de la classe CSommet
/*************************************************************************
****** Entr�e : Rien
****** Sortie : Rien
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
CSommet::CSommet() : ARCSOMArcsArrivants(), ARCSOMArcsPartants()
{
	uiSOMID = 0;
}

/*************************************************************************
Constructeur de la classe CSommet avec ID sp�cifi�
/*************************************************************************
****** Entr�e : Entier non sign� (uiID) repr�sentant l'ID du sommet
****** Sortie : Rien
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
CSommet::CSommet(unsigned int uiID) : ARCSOMArcsArrivants(), ARCSOMArcsPartants()
{
	uiSOMID = uiID;
}

/*************************************************************************
Constructeur de copie de la classe CSommet
/*************************************************************************
****** Entr�e : Objet CSommet (SOMArg) � copier
****** Sortie : Rien
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
CSommet::CSommet(CSommet & SOMArg)
{
	*this = SOMArg;
}

/*************************************************************************
Destructeur de la classe CSommet
/*************************************************************************
****** Entr�e : Rien
****** Sortie : Rien
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
CSommet::~CSommet() {}

/*************************************************************************
Accesseur pour obtenir l'ID du sommet
/*************************************************************************
****** Entr�e : Rien
****** Sortie : Entier non sign� repr�sentant l'ID du sommet
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
unsigned int CSommet::SOMGetID() const
{
	return uiSOMID;
}

/*************************************************************************
Accesseur pour obtenir un arc arrivant sp�cifique
/*************************************************************************
****** Entr�e : Entier non sign� (uiPlace) repr�sentant la position de l'arc
****** dans le tableau ARCSOMArcsArrivants
****** Sortie : Pointeur vers l'objet CArc correspondant � l'arc arrivant
****** � la position sp�cifi�e
****** Pr�condition : Rien
****** Postcondition : Rien
*************************************************************************/
CArc* CSommet::ARCSOMGetArcsArrivants(unsigned int uiPlace) 
{
	return ARCSOMArcsArrivants.TABGetElement(uiPlace);
}

/*************************************************************************
Accesseur pour obtenir un arc partant sp�cifique
/*************************************************************************
****** Entr�e : Entier non sign� (uiPlace) repr�sentant la position de l'arc
****** dans le tableau ARCSOMArcsPartants
****** Sortie : Pointeur vers l'objet CArc correspondant � l'arc partant
****** � la position sp�cifi�e
****** Pr�condition : Rien
****** Postcondition : Rien
*************************************************************************/
CArc* CSommet::ARCSOMGetArcsPartants(unsigned int uiPlace) 
{
	return ARCSOMArcsPartants.TABGetElement(uiPlace);
}

/*************************************************************************
Accesseur en �criture pour d�finir l'ID du sommet
/*************************************************************************
****** Entr�e : Entier non sign� (uiID) repr�sentant l'ID du sommet
****** Sortie : Rien
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
void CSommet::SOMSetID(unsigned int uiID)
{
	uiSOMID = uiID;
}

/*************************************************************************
Obtenir le nombre d'arcs arrivants
/*************************************************************************
****** Entr�e : Rien
****** Sortie : Entier non sign� repr�sentant le nombre d'arcs arrivants
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
unsigned int CSommet::SOMGetNbsArcArrivants()
{
	return ARCSOMArcsArrivants.TABGetTaille();
}

/*************************************************************************
Obtenir le nombre d'arcs partants
/*************************************************************************
****** Entr�e : Rien
****** Sortie : Entier non sign� repr�sentant le nombre d'arcs partants
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
unsigned int CSommet::SOMGetNbsArcPartants()
{
	return ARCSOMArcsPartants.TABGetTaille();
}

/*************************************************************************
Obtenir un tableau des destinations des arcs partants
/*************************************************************************
****** Entr�e : Rien
****** Sortie : Objet CTableauDynamique<int> contenant les destinations
****** des arcs partants
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
CTableauDynamique<int> CSommet::TABSOMTableauDestinationArcsPartants()
{
	unsigned int uiBoucle;
	CTableauDynamique<int> TableauDestinationPartants;
	for (uiBoucle = 0; uiBoucle < this->SOMGetNbsArcPartants(); uiBoucle++) {
		TableauDestinationPartants.TABAddElement(ARCSOMGetArcsPartants(uiBoucle)->ARCgetDestination());
	}
	return TableauDestinationPartants;
}

/*************************************************************************
Obtenir un tableau des destinations des arcs arrivants
/*************************************************************************
****** Entr�e : Rien
****** Sortie : Objet CTableauDynamique<int> contenant les destinations
****** des arcs arrivants
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
CTableauDynamique<int> CSommet::TABSOMTableauDestinationArcsArrivants()
{
	unsigned int uiBoucle;
	CTableauDynamique<int> TableauDestinationArcsArrivants;
	for (uiBoucle = 0; uiBoucle < this->SOMGetNbsArcArrivants(); uiBoucle++) {
		TableauDestinationArcsArrivants.TABAddElement(ARCSOMGetArcsArrivants(uiBoucle)->ARCgetDestination());
	}
	return TableauDestinationArcsArrivants;
}

/*************************************************************************
Ajoute un arc arrivant au sommet
/*************************************************************************
****** Entr�e : Entier non sign� repr�sentant l'ID de la destination de l'arc
****** Sortie : Rien
****** Pr�condition : Rien
****** Postcondition : L'arc arrivant est ajout� au sommet s'il n'existe pas d�j�
**************************************************************************/
void CSommet::SOMAjouterArcArrivant(unsigned int uiDestination)
{
	
	if ( this->SOMTrouverDestinationArcArrivant(uiDestination) == -1 )
	{
		CArc* Temp = new CArc(uiDestination);
		ARCSOMArcsArrivants.TABAddElement(Temp);
	}
	
}

/*************************************************************************
Supprime un arc partant du sommet
/*************************************************************************
****** Entr�e : Entier non sign� repr�sentant l'ID de la destination de l'arc
****** Sortie : Rien
****** Pr�condition : Rien
****** Postcondition : L'arc partant est supprim� du sommet s'il existe
**************************************************************************/
void CSommet::SOMSupprimerArcPartant(unsigned int uiDestination)
{
	unsigned int uiEmplacement = this->SOMTrouverDestinationArcPartant(uiDestination);
	if (uiEmplacement != -1) {
		ARCSOMArcsPartants.TABDeleteElement( uiEmplacement );
	}
}

/*************************************************************************
Ajoute un arc partant au sommet
/*************************************************************************
****** Entr�e : Entier non sign� repr�sentant l'ID de la destination de l'arc
****** Sortie : Rien
****** Pr�condition : Rien
****** Postcondition : L'arc partant est ajout� au sommet s'il n'existe pas d�j�
**************************************************************************/
void CSommet::SOMAjouterArcPartant(unsigned int uiDestination)
{
	if (this->SOMTrouverDestinationArcPartant(uiDestination) == -1) {
		CArc* Temp = new CArc(uiDestination);
		ARCSOMArcsPartants.TABAddElement(Temp);
	}
}

/*************************************************************************
Supprime un arc arrivant du sommet
/*************************************************************************
****** Entr�e : Entier non sign� repr�sentant l'ID de la destination de l'arc
****** Sortie : Rien
****** Pr�condition : Rien
****** Postcondition : L'arc arrivant est supprim� du sommet s'il existe
**************************************************************************/
void CSommet::SOMSupprimerArcArrivant(unsigned int uiDestination)
{
	unsigned int uiEmplacement = this->SOMTrouverDestinationArcArrivant(uiDestination);
	if (uiEmplacement != -1) {
		ARCSOMArcsArrivants.TABDeleteElement(uiEmplacement);
	}
}

/*************************************************************************
Trouve l'emplacement d'une destination d'un arc partant
/*************************************************************************
****** Entr�e : Entier non sign� repr�sentant l'ID de la destination de l'arc
****** Sortie : Entier repr�sentant l'emplacement de la destination dans le tableau d'arcs partants (ou -1 si non trouv�)
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
int CSommet::SOMTrouverDestinationArcPartant(unsigned int uiDestination)
{
	unsigned int uiBoucle1;
	for (uiBoucle1 = 0; uiBoucle1 < ARCSOMArcsPartants.TABGetTaille(); uiBoucle1++) {
		if (ARCSOMArcsPartants.TABGetElement(uiBoucle1)->ARCgetDestination() == uiDestination) {
			return uiBoucle1;
		}
	}
	/*CException EXCLevee(EXCIdSommetNotExist, "Cet Id ne correspond a aucun sommet");
	throw(EXCLevee);*/
	return -1;
}

/*************************************************************************
Trouve l'emplacement d'une destination d'un arc arrivant
/*************************************************************************
****** Entr�e : Entier non sign� repr�sentant l'ID de la destination de l'arc
****** Sortie : Entier repr�sentant l'emplacement de la destination dans le tableau d'arcs arrivants (ou -1 si non trouv�)
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
int CSommet::SOMTrouverDestinationArcArrivant(unsigned int uiDestination)
{
	unsigned int uiBoucle1;
	for (uiBoucle1 = 0; uiBoucle1 < ARCSOMArcsArrivants.TABGetTaille(); uiBoucle1++) {
		if (ARCSOMArcsArrivants.TABGetElement(uiBoucle1)->ARCgetDestination() == uiDestination) {
			return uiBoucle1;
		}
	}
	/*CException EXCLevee(EXCIdSommetNotExist, "Cet Id ne correspond a aucun sommet");
	throw(EXCLevee);*/
	return -1;
}

/*************************************************************************
Op�rateur d'affectation de la classe CSommet
/*************************************************************************
****** Entr�e : Objet CSommet (SOMArg) � copier
****** Sortie : R�f�rence vers l'objet CSommet actuel apr�s la copie
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
CSommet& CSommet::operator=(CSommet & SOMArg)
{
	uiSOMID = SOMArg.uiSOMID;

	ARCSOMArcsArrivants.operator=(SOMArg.ARCSOMArcsArrivants);
	ARCSOMArcsPartants.operator=(SOMArg.ARCSOMArcsPartants);

	return *this;
}
