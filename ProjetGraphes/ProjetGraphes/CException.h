#pragma once
#ifndef CEXCEPTION_H
#define CEXCEPTION_H

#define EXCErreurParDefaut 0

#include <iostream>

class CException
{
private:
	/*******************
		   ATTRIBUT
	********************/
	unsigned int uiEXCvaleur;
	char cEXCErreur[1000];

public:
	/*******************
		CONSTRUCTEURS
	********************/

	/*************************************************************************
				Constructeur par defaut de la classe CException
	**************************************************************************
	******		Entree :		Rien
	******		Sortie :		Rien
	******		Precondition :	Rien
	******		Postcondition : Rien
	**************************************************************************/
	CException();


	/*************************************************************************
				Destructeur par defaut de la classe CException
	**************************************************************************
	******		Entree :		uiEXobjet -> Le numero de l'erreur
	******						cEXCobjet -> Chaine de caractere pour stocker le message d'erreur asscocie a l'erreur
	******		Sortie :		Rien
	******		Precondition :	Rien
	******		Postcondition : Rien
	**************************************************************************/
	CException(unsigned int uiEXCobjet, const char cEXCobjet[1000]);


	/*******************
		DESTRUCTEUR
	********************

/*************************************************************************
			Destructeur par defaut de la classe CException
**************************************************************************
******		Entree :		Rien
******		Sortie :		Rien
******		Precondition :	Rien
******		Postcondition : Rien
**************************************************************************/
	~CException();


	/*******************
		   GETTER
	********************/

	/*************************************************************************
				Accesseur en lecture du code d'erreur
	**************************************************************************
	******		Entree :		Rien
	******		Sortie :		Code d'erreur
	******		Precondition :  Rien
	******		Postcondition : Rien
	**************************************************************************/
	int EXClire_valeur();


	/*******************
		   METHODE
	********************/

	/*************************************************************************
					Afficher l'erreur
	**************************************************************************
	******		Entree :		Rien
	******		Sortie :		Rien
	******		Precondition :  Rien
	******		Postcondition : Rien
	**************************************************************************/
	void EXCAfficherErreur();

};
#endif 