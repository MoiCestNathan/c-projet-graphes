#include "CParseurGraphe.h"

/*************************************************************************
M�thode pour g�n�rer le Graphe
/*************************************************************************
****** Entr�e : Chemin du fichier
****** Sortie : Objet CGraphe
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
CGraphe CParseurGraphe::GRAGenerer(const char * CheminFichier)
{
	// D�claration des objets CGraphe et CParseur
	CGraphe graphe;
	CParseur parseur;

	// D�claration des variables
	unsigned int uiNBSommets = 0;
	unsigned int uiNBArcs = 0;
	unsigned int uiBoucle = 0;

	unsigned int uiNumSommet = 0;

	unsigned int uiNumSommetDebut = 0;
	unsigned int uiNumSommetFin = 0;

	char** ppcTableauSommets;
	char** ppcTableauArcsDebut;
	char** ppcTableauArcsFin;

	// D�claration des chaines de caract�res pour parser le fichier
	char cChaineCaracSommets[] = "nbsommets";
	char cChaineCaracArcs[] = "nbarcs";

	char cChaineCaracS[] = "sommets";
	char cChaineCaracA[] = "arcs";

	char cChaineN[] = "numero";
	char cChaineD[] = "debut";
	char cChaineF[] = "fin";

	// Conversions chaine de caract�res en une valeur num�rique 
	uiNBSommets = atoi(parseur.PARParserPartieSimple(CheminFichier, cChaineCaracSommets));
	uiNBArcs = atoi(parseur.PARParserPartieSimple(CheminFichier, cChaineCaracArcs));

	// R�cup�rer valeurs du fichier
	ppcTableauSommets = parseur.PARParserPartieComplexe(CheminFichier, cChaineCaracS, uiNBSommets, cChaineN);
	ppcTableauArcsDebut = parseur.PARParserPartieComplexe(CheminFichier, cChaineCaracA, uiNBArcs, cChaineD);
	ppcTableauArcsFin = parseur.PARParserPartieComplexe(CheminFichier, cChaineCaracA, uiNBArcs, cChaineF);

	// Ajouter Sommets
	for (uiBoucle = 0; uiBoucle < uiNBSommets; uiBoucle++) {
		uiNumSommet = atoi(ppcTableauSommets[uiBoucle]);
		graphe.GRAAjouterSommet(uiNumSommet);
	}

	// Ajouter Arcs
	for (uiBoucle = 0; uiBoucle < uiNBArcs; uiBoucle++) {
		uiNumSommetDebut = atoi(ppcTableauArcsDebut[uiBoucle]);
		uiNumSommetFin = atoi(ppcTableauArcsFin[uiBoucle]);
		graphe.GRAAjouterArc(uiNumSommetDebut, uiNumSommetFin);
	}

	return graphe;
}
