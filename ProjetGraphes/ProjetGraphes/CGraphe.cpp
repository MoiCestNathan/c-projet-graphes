#include "CGraphe.h"

/*************************************************************************
Constructeur par d�faut de la classe CGraphe
/*************************************************************************
****** Entr�e : Rien
****** Sortie : Rien
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
CGraphe::CGraphe() : TABGRASommets()
{
	bOriente = true;
}

/*************************************************************************
Constructeur de la classe CGraphe avec param�tre
/*************************************************************************
****** Entr�e : bool�en bOrientation pour sp�cifier l'orientation du graphe
****** Sortie : Rien
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
CGraphe::CGraphe(bool bOriantation) : TABGRASommets()
{
	bOriente = bOriantation;
}

/*************************************************************************
Constructeur de copie de la classe CGraphe
/*************************************************************************
****** Entr�e : Objet CGraphe (GRAArg) � copier
****** Sortie : Rien
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
CGraphe::CGraphe(CGraphe &GRAArg) 
{
	TABGRASommets = GRAArg.TABGRASommets;
	bOriente = GRAArg.bOriente;
}

/*************************************************************************
Destructeur de la classe CGraphe
/*************************************************************************
****** Entr�e : Rien
****** Sortie : Rien
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
CGraphe::~CGraphe() {}

/*************************************************************************
Accesseur pour obtenir l'orientation du graphe
/*************************************************************************
****** Entr�e : Rien
****** Sortie : Bool�en repr�sentant l'orientation du graphe
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
bool CGraphe::GRAGetOrienation() {
	return bOriente;
}

/*************************************************************************
Accesseur en �criture pour d�finir l'orientation du graphe
/*************************************************************************
****** Entr�e : Bool�en repr�sentant la nouvelle orientation du graphe
****** Sortie : Rien
****** Pr�condition : Rien
****** Postcondition : Les arcs du graphe peuvent �tre modifi�s selon la nouvelle orientation
**************************************************************************/
void CGraphe::GRASetOrientation(bool bChangement) {
	
	unsigned int uiBoucle , uiBoucle2, uiIdSommet;
	CTableauDynamique<int> TableauDestinationArc;
	

	if (bOriente != bChangement) {
		if (bChangement == false) {
			for (uiBoucle = 0; uiBoucle < TABGRASommets.TABGetTaille(); uiBoucle++) {
				
				uiIdSommet = this->GRAGetIdSommet(uiBoucle);
				
				if (this->TABGRATableauDestinationArcsPartants(uiIdSommet).TABGetTaille() != 0) {
					TableauDestinationArc = (CTableauDynamique<int>&)(this->TABGRATableauDestinationArcsPartants(uiIdSommet));
					for (uiBoucle2 = 0; uiBoucle2 < TableauDestinationArc.TABGetTaille(); uiBoucle2++) {
						this->GRAAjouterArc(uiIdSommet, *(TableauDestinationArc.TABGetElement(uiBoucle2)));
					}
				}			
				if (this->TABGRATableauDestinationArcsArrivants(uiIdSommet).TABGetTaille() != 0) {
					TableauDestinationArc = (CTableauDynamique<int>&)(this->TABGRATableauDestinationArcsArrivants(uiIdSommet));
					for (uiBoucle2 = 0; uiBoucle2 < TableauDestinationArc.TABGetTaille(); uiBoucle2++) {
						this->GRAAjouterArc(uiIdSommet , *(TableauDestinationArc.TABGetElement(uiBoucle2)));
					}					
				}
			}
		}
		bOriente = bChangement;
	}
}

/*************************************************************************
Accesseur pour obtenir le nombre de sommets du graphe
/*************************************************************************
****** Entr�e : Rien
****** Sortie : Entier non sign� repr�sentant le nombre de sommets
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
unsigned int CGraphe::GRAGetNbSommets() 
{
	return TABGRASommets.TABGetTaille();
}

/*************************************************************************
Accesseur pour obtenir un sommet du graphe
/*************************************************************************
****** Entr�e : Entier non sign� repr�sentant la position du sommet dans le tableau
****** Sortie : Pointeur vers l'objet CSommet correspondant au sommet demand�
****** Pr�condition : Rien
****** Postcondition : Rien
*************************************************************************/
CSommet* CGraphe::GRAGetSommet(unsigned int uiPos) 
{
	return TABGRASommets.TABGetElement(uiPos);
}

/*************************************************************************
Accesseur pour obtenir l'ID d'un sommet du graphe
/*************************************************************************
****** Entr�e : Position du sommet dans le tableau de sommets (uiPos)
****** Sortie : Entier repr�sentant l'ID du sommet
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
int CGraphe::GRAGetIdSommet(unsigned int uiPos)
{
	return this->GRAGetSommet(uiPos)->SOMGetID();
}

/*************************************************************************
Changer l'ID d'un sommet du graphe
/*************************************************************************
****** Entr�e : ID actuel du sommet (uiIdSommet), nouvel ID du sommet (uiNouvelId)
****** Sortie : Rien
****** Pr�condition : iEmplacement != -1 | Le sommet doit �tre pr�sent
****** Postcondition : Rien
**************************************************************************/
void CGraphe::GRAChangeIdSommet(unsigned int uiIdSommet, unsigned int uiNouvelId) {
	int iEmplacement = this->GRATrouverId(uiIdSommet);
	unsigned int uiBoucle;
	CTableauDynamique<int> TableauDestinationArc;
	if(iEmplacement != -1) {
		this->GRAAjouterSommet(uiNouvelId);
		if (this->TABGRATableauDestinationArcsPartants(uiIdSommet).TABGetTaille() != 0) {
			TableauDestinationArc = (CTableauDynamique<int>&)(this->TABGRATableauDestinationArcsArrivants(uiIdSommet));
			for (uiBoucle = 0; uiBoucle < TableauDestinationArc.TABGetTaille(); uiBoucle++) {
				this->GRAAjouterArc(uiNouvelId, *TableauDestinationArc.TABGetElement(uiBoucle));
			}
		}

		/*if (this->TABGRATableauDestinationArcsArrivants(uiIdSommet).TABGetTaille() != 0) {
			TableauDestinationArc = (CTableauDynamique<int>&)(this->TABGRATableauDestinationArcsArrivants(uiIdSommet));
			for (uiBoucle = 0; uiBoucle < TableauDestinationArc.TABGetTaille(); uiBoucle++) {
				this->GRAAjouterArc(uiNouvelId, *TableauDestinationArc.TABGetElement(uiBoucle));
			}

		}*/

		this->GRASupprimerSommet(uiIdSommet);
	}
	else {
		CException EXCLevee( EXCSommetNotFound , "Le Sommet que vous cherchez a modifier n'existe pas");
		throw(EXCLevee);
	}
}

/*************************************************************************
Ajouter un sommet au graphe
/*************************************************************************
****** Entr�e : ID du sommet � ajouter (uiIDSommet)
****** Sortie : Rien
****** Pr�condition : uiIDSommet > 0
****** Postcondition : Rien
*************************************************************************/
void CGraphe::GRAAjouterSommet(unsigned int uiIDSommet)
{
	if (uiIDSommet >= 0 && this->GRATrouverId(uiIDSommet) == -1) {
		CSommet* SOMTemps = new CSommet(uiIDSommet);
		TABGRASommets.TABAddElement(SOMTemps);
	}
	else if (uiIDSommet < 0) {
		CException EXCLevee(EXCInvalidSommetId, " L'Id de votre Sommet doit etre un entier positif et etre different de ceux deja existant");
		throw(EXCLevee);
	}
}

/*************************************************************************
Supprimer un sommet du graphe
/*************************************************************************
****** Entr�e : ID du sommet � supprimer (uiIdSommetASupprimer)
****** Sortie : Rien
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
void CGraphe::GRASupprimerSommet(unsigned int uiIdSommetASupprimer)
{
	unsigned int uiBoucle = 0;
	int  iEmplacement = this->GRATrouverId(uiIdSommetASupprimer);
	if (iEmplacement != -1) {
		
		TABGRASommets.TABDeleteElement(iEmplacement);
		for (uiBoucle = 0; uiBoucle < TABGRASommets.TABGetTaille(); uiBoucle++) {
			TABGRASommets.TABGetElement(uiBoucle)->SOMSupprimerArcPartant(uiIdSommetASupprimer);
			TABGRASommets.TABGetElement(uiBoucle)->SOMSupprimerArcArrivant(uiIdSommetASupprimer);
		}
	}
}

/*************************************************************************
Ajouter un arc entre deux sommets du graphe
/*************************************************************************
****** Entr�e : ID du sommet de d�part (Depart), ID du sommet d'arriv�e (Arrive)
****** Sortie : Rien
****** Pr�condition :	iEmplacementArrive != -1 
******					iEmplacementDepart != -1
****** Postcondition : Rien
**************************************************************************/
void CGraphe::GRAAjouterArc(unsigned int Depart, unsigned int Arrive) {
	
	int iEmplacementDepart = this->GRATrouverId(Depart),  iEmplacementArrive = this->GRATrouverId(Arrive) ;
	
	if (iEmplacementArrive != -1 && iEmplacementDepart != -1) {
		TABGRASommets.TABGetElement(iEmplacementDepart)->SOMAjouterArcPartant(Arrive);
		TABGRASommets.TABGetElement(iEmplacementArrive)->SOMAjouterArcArrivant(Depart);

		if (bOriente == false) {
			TABGRASommets.TABGetElement(iEmplacementDepart)->SOMAjouterArcArrivant(Arrive);
			TABGRASommets.TABGetElement(iEmplacementArrive)->SOMAjouterArcPartant(Depart);
		}
	}
	
	else if (iEmplacementArrive == -1) {
		CException EXCLevee( EXCIDSommetDepartNonExistant , "L'Id du Sommet saisie comme Sommet de depart n'existe pas");
		throw(EXCLevee);
	}
	
	else if (iEmplacementDepart == -1) {
		CException EXCLevee(EXCIDSommetArriveNonExistant, "L'Id du Sommet saisie comme Sommet d'arrive n'existe pas");
		throw(EXCLevee);
	}
}

/*************************************************************************
Supprimer un arc entre deux sommets du graphe
/*************************************************************************
****** Entr�e : ID du sommet de d�part (Depart), ID du sommet d'arriv�e (Arrive)
****** Sortie : Rien
****** Pr�condition :	iEmplacementArrive != -1 
******					iEmplacementDepart != -1
****** Postcondition : Rien
**************************************************************************/
void CGraphe::GRADeleteArc(unsigned int Depart, unsigned int Arrive) {
	
		int iEmplacementDepart = this->GRATrouverId(Depart),  iEmplacementArrive = this->GRATrouverId(Arrive) ;
	
	if (iEmplacementArrive != -1 && iEmplacementDepart != -1) {
		TABGRASommets.TABGetElement(iEmplacementDepart)->SOMSupprimerArcPartant(Arrive);
		TABGRASommets.TABGetElement(iEmplacementArrive)->SOMSupprimerArcArrivant(Depart);

		if (bOriente == false) {
			TABGRASommets.TABGetElement(iEmplacementDepart)->SOMSupprimerArcArrivant(Arrive);
			TABGRASommets.TABGetElement(iEmplacementArrive)->SOMSupprimerArcPartant(Depart);
		}
	}
	
	else if (iEmplacementArrive == -1) {
		CException EXCLevee( EXCIDSommetDepartNonExistant , "L'Id du Sommet saisie comme Sommet de depart n'existe pas");
		throw(EXCLevee);
	}
	
	else if (iEmplacementDepart == -1) {
		CException EXCLevee(EXCIDSommetArriveNonExistant, "L'Id du Sommet saisie comme Sommet d'arrive n'existe pas");
		throw(EXCLevee);
	}
}

/*************************************************************************
Afficher le graphe
**************************************************************************
****** Entr�e : Rien
****** Sortie : Rien
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
void CGraphe::GRAAfficherGraphe() 
{
	int ivers;
	unsigned int uiBoucle1 = 0, uiBoucle2 = 0;
	CTableauDynamique<int> TableauDestination ;
	if (TABGRASommets.TABGetTaille() == 0) {
		cout << "Graphe est vide" << endl;
	}
	for (uiBoucle1 = 0; uiBoucle1 < TABGRASommets.TABGetTaille(); uiBoucle1++) {

		if ( TABGRASommets.TABGetElement(uiBoucle1) != nullptr) {
			CSommet* SOMSommet = TABGRASommets.TABGetElement(uiBoucle1) ;

			cout << "Sommet " << SOMSommet->SOMGetID() << " :" << endl;
			if (SOMSommet->SOMGetNbsArcPartants() != 0) {
				cout << "Arcs partants :\n";
				TableauDestination = (CTableauDynamique<int>&)SOMSommet->TABSOMTableauDestinationArcsPartants();
				for (uiBoucle2 = 0; uiBoucle2 < SOMSommet->SOMGetNbsArcPartants(); uiBoucle2++) {
					
					if (bOriente == true) {
						cout << SOMSommet->SOMGetID() << "------>" << *TableauDestination.TABGetElement(uiBoucle2) << endl;
					}
					else {
						cout << SOMSommet->SOMGetID() << "-------" << *TableauDestination.TABGetElement(uiBoucle2) << endl;
					}
				}
			}
			else {
				cout << "Ce sommet ne possede aucun arc sortant" << endl;
			}
			cout << "\n" << endl;
		}
	}
}

/*************************************************************************
Trouver l'ID d'un sommet
**************************************************************************
****** Entr�e : Entier non sign� repr�sentant l'ID du sommet � trouver
****** Sortie : Entier non sign� repr�sentant l'indice du sommet dans le tableau ou -1
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
unsigned int CGraphe::GRATrouverId(unsigned int uiIdATrouver) 
{

	unsigned int uiBoucle1;
	for (uiBoucle1 = 0; uiBoucle1 < TABGRASommets.TABGetTaille(); uiBoucle1++) {
		if (TABGRASommets.TABGetElement(uiBoucle1)->SOMGetID() == uiIdATrouver) {
			return uiBoucle1;
		}
	}
	return -1;
}

/*************************************************************************
Obtenir un tableau des IDs des sommets
**************************************************************************
****** Entr�e : Rien
****** Sortie : Objet CTableauDynamique<int> contenant les IDs des sommets
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
CTableauDynamique<int> CGraphe::TABGRATableauIDSommets()
{
	unsigned int uiBoucle;
	CTableauDynamique<int> TableauIds;
	for (uiBoucle = 0; uiBoucle < TABGRASommets.TABGetTaille(); uiBoucle++) {
		TableauIds.TABAddElement( (TABGRASommets.TABGetElement(uiBoucle)->SOMGetID()) );
	}
	return TableauIds;
}

/*************************************************************************
Obtenir un tableau des destinations des arcs partants d'un sommet
**************************************************************************
****** Entr�e : Entier non sign� repr�sentant l'ID du sommet
****** Sortie : Objet CTableauDynamique<int> contenant les destinations des arcs partants
****** Pr�condition : iEmplacement != -1 | Le sommet doit �tre pr�sent
****** Postcondition : Rien
**************************************************************************/
CTableauDynamique<int> CGraphe::TABGRATableauDestinationArcsPartants(unsigned int uiIdSommet)
{
	int iEmplacement = this->GRATrouverId(uiIdSommet);
	if (iEmplacement != -1)
	{
		return (CTableauDynamique<int>&) TABGRASommets.TABGetElement(iEmplacement)->TABSOMTableauDestinationArcsPartants();
	}
	else {
		CException EXCLevee(EXCIDSommetNonExistant, "L'Id du Sommet saisie n'existe pas");
		throw(EXCLevee);
	}
}

/*************************************************************************
Obtenir un tableau des destinations des arcs arrivants � un sommet
**************************************************************************
****** Entr�e : Entier non sign� repr�sentant l'ID du sommet
****** Sortie : Objet CTableauDynamique<int> contenant les destinations des arcs arrivants
****** Pr�condition : iEmplacement != -1 | Le sommet doit �tre pr�sent
****** Postcondition : Rien
**************************************************************************/
CTableauDynamique<int> CGraphe::TABGRATableauDestinationArcsArrivants(unsigned int uiIdSommet)
{
	int iEmplacement = this->GRATrouverId(uiIdSommet);
	if (iEmplacement != -1) {
		return (CTableauDynamique<int>&)(TABGRASommets.TABGetElement(iEmplacement)->TABSOMTableauDestinationArcsArrivants());
	}
	else {
		CException EXCLevee(EXCIDSommetNonExistant, "L'Id du Sommet saisie n'existe pas");
		throw(EXCLevee);
	}
}

/*************************************************************************
Op�rateur d'affectation de la classe CGraphe
**************************************************************************
****** M�thode utilis�e : Op�rateur d'affectation
****** Entr�e : R�f�rence vers un objet CGraphe (GRAArg) � copier
****** Sortie : R�f�rence vers l'objet CGraphe actuel apr�s la copie
****** Pr�condition : Rien
****** Postcondition : Rien
**************************************************************************/
CGraphe& CGraphe::operator=(CGraphe &GRAArg) {
	
	TABGRASommets =  GRAArg.TABGRASommets ;
	bOriente = GRAArg.bOriente;

	return *this;
}

